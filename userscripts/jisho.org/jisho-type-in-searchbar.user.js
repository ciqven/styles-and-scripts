// ==UserScript==
// @name         Jisho.org type in search bar
// @namespace    http://ciqven.com
// @version      0.3
// @description  Focus on searchbox when typing on jisho page
// @author       çiqvĕɳ tyoⱶĭnƺĕ
// @match        https://jisho.org/search/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=jisho.org
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    // A list of keys to ignore
    const ignoreList = [
        91, // OS (Super, windows key)
        8, // Backspace
        9, // Tab
    ];

    // Find search box <input> element
    const editBox = document.querySelector('#keyword');

    // Register a page wide handler
    document.addEventListener('keydown', event => {
        // Ignore presses when alt or control are down
        if (event.altKey || event.ctrlKey) {
            return;
        }

        // Ignore IME composing
        if (event.isComposing || event.keyCode === 229) {
            return;
        }

        // If key is in ignore list, skip
        if (ignoreList.includes(event.keyCode)) {
            return;
        }

        // If already focused, do not proceed
        if (document.activeElement === editBox) {
            return;
        }

        //Select all to overwrite
        editBox.setSelectionRange(0, editBox.value.length);

        // Focus on searchbox
        editBox.focus();
    });
})();